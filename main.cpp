#include "View/mainwindow.h"
#include "View/contabilitawindow.h"
#include <QApplication>
#include "Model/container.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

        //SET CONTAINER
    container* totale= new container();
    totale->popolamento();


    MainWindow w(totale);
    w.show();
    return a.exec();
}
