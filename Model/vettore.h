#ifndef VETTORE_H
#define VETTORE_H

#include<iostream>

template <class T>
class Vettore{

    private:

        T* ar;
        unsigned int _size;
        unsigned int obj_count;

        void resize(){

            T* nuovo_ar;
            unsigned int nuova_size = _size == 0 ? 1 : _size * 2;
            nuovo_ar = new T[nuova_size];
            for(unsigned int i=0; i<_size; ++i){
                nuovo_ar[i] = ar[i];
            }
            delete[] ar;
            ar = nuovo_ar;
            _size = nuova_size;
        }

        void shift(int start){
            for(unsigned int i=start; i<obj_count-1; ++i){
                ar[i] = ar[i+1];
            }
        }


    public:

        unsigned int size() const{return _size;}
        unsigned int count() const{return obj_count;}

        //CONSTRUCTORS
        Vettore(unsigned int s=3) : ar(s == 0 ? nullptr : new T[s]) , _size(s), obj_count(0){}

        Vettore(const Vettore& v) : ar(v.size() == 0 ? nullptr : new T[v.size()]) , _size(v.size()), obj_count(v.count()){

            for(unsigned int i=0; i<_size; ++i){
                ar[i] = v.ar[i];
            }

        }

        //OPERATORS
        Vettore &operator=(const Vettore &v){

            if(this != &v){
                delete[] ar;
                ar = v.size() == 0 ? nullptr : new T[v.size()];
                _size = v.size();
                obj_count = v.count();
                for(unsigned int i=0; i<_size; ++i){
                ar[i] = v[i];
                }
            }

            return *this;
        }

        bool operator==(const Vettore& v) const{

            if(this == &v) return true;

            if(_size != v.size()) return false;

            for(int i=0; i<_size; ++i){
                if(ar[i] != v[i]) return false;
            }

            return true;
        }

        T operator[](unsigned int i) const{        
            return ar[i];
        }


        //METHODS
        bool empty() const{
            if(obj_count == 0)
                return true;
            return false;
        }

        bool full() const{
            if(obj_count == _size)
                return true;
            return false;
        }

        T* first() const{
            return ar;
        }

        T* last() const{
           return ar + obj_count - 1;
        }

        void push_back(const T& t){

            if(full()) resize();
            ar[obj_count] = t;
            obj_count++;

        }

        void remove(const T& t){
            for(unsigned int i=0; i<obj_count; ++i){
                if(ar[i] == t){
                    delete ar[i];
                    shift(i);
                    obj_count--;
                }
            }
        }

        void append(const Vettore& v){

            unsigned int nuova_size = obj_count + v.count();
            Vettore nuovo_vett(nuova_size);

            for(int i=0; i<obj_count; ++i){
                nuovo_vett[i] = ar[i];
            }
            for(int i=obj_count; i<nuova_size; ++i){
                nuovo_vett[i] = v[i-obj_count];
            }

            *this = nuovo_vett;

        }

        class iterator{
            friend class Vettore<T>;
            friend class container;
            private:
                T* ptr;

                iterator(T* p) : ptr(p){}

            public:

                //CONSTRUCTORS
                iterator() : ptr(nullptr){}

                //OPERATORS
                T& operator*() const{
                    return *ptr;
                }

                T* operator->() const{
                    return ptr;
                }

                bool operator==(const iterator& i) const{
                    return ptr == i.ptr;
                }

                bool operator!=(const iterator& i) const{
                    return ptr!=i.ptr;
                }

                iterator& operator++(){
                    //if(ptr!=nullptr){
                        ++ptr;
                    //}

                    return *this;
                }

                iterator operator++(int){

                    iterator aux = *this;
                    if(ptr!=nullptr){
                        ++ptr;
                    }

                    return aux;
                }

                iterator& operator--(){

                    if(ptr!=nullptr){
                        --ptr;
                    }

                    return *this;
                }

                iterator operator--(int){

                    iterator aux = *this;
                    if(ptr!=nullptr){
                       --ptr;
                    }

                    return aux;
                }

        };

        class const_iterator{
            friend class Vettore<T>;
            friend class container;
            private:

                T* ptr;

                const_iterator(T* p) : ptr(p){}

            public:

                const T& operator*() const{
                    return *ptr;
                }

                const T* operator->() const{
                    return ptr;
                }

                bool operator==(const const_iterator& i) const{
                    return ptr == i.ptr;
                }

                bool operator!=(const const_iterator& i) const{
                    return ptr!=i.ptr;
                }

                const_iterator& operator++(){
                    if(ptr!=nullptr){
                        ++ptr;
                    }

                    return *this;
                }

                const_iterator operator++(int){

                    const_iterator aux = *this;
                    if(ptr!=nullptr){
                       ++ptr;
                    }

                    return aux;
                }

                const_iterator& operator--(){

                    if(ptr!=nullptr){
                        --ptr;
                    }

                    return *this;
                }

                const_iterator operator--(int){

                    const_iterator aux = *this;
                    if(ptr!=nullptr){
                        --ptr;
                    }

                    return aux;
                }
        };

        iterator begin() const{
            return ar;
        }

        iterator end() const{
            if(_size == 0) return nullptr;
            return iterator(last()+1);
        }

        const_iterator cbegin() const{
            return ar;
        }

        const_iterator cend() const{
            if(_size == 0) return nullptr;
            return const_iterator(last()+1);
        }



        //DESTRUCTOR
        ~Vettore(){delete[] ar;}


};

#endif // Vettore_H
