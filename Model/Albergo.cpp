#include "Albergo.h"


//COSTRUTTORE
Albergo::Albergo(string no, string ti, string in, string ci, string na, unsigned int numsta, unsigned int st, unsigned int quo, unsigned int pre, bool co)
:ImpresaRicettiva(no, ti, in, ci, na),
numeroStanze(numsta),
stelle(st),
colazione(co),
quotaPerPrenotazione(quo),
prenotazioniMeseCorrente(pre){}



void Albergo::resetMese(){
        prenotazioniMeseCorrente=0;
}


void Albergo::incrementa(unsigned int tot){
    prenotazioniMeseCorrente+=tot;
}

double Albergo::fatturatoMeseCorrente() const{
    return prenotazioniMeseCorrente*quotaPerPrenotazione;
}


                                    //GETTERS

unsigned int Albergo::getQuotaPerPrenotazione() const { return quotaPerPrenotazione;}
unsigned int Albergo::getPrenotazioniMeseCorrente() const { return prenotazioniMeseCorrente;}
unsigned int Albergo::getNumeroStanze() const{ return numeroStanze;}
unsigned int Albergo::getStelle() const{ return stelle;}
bool Albergo::getColazione() const{ return colazione;}




                                    //SETTERS

void Albergo::setNumeroStanze(unsigned int temp){ numeroStanze=temp;}
void Albergo::setStelle(unsigned int temp){ stelle=temp;}
void Albergo::setColazione(bool temp){ colazione=temp;}
void Albergo::setQuotaPerPrenotazione(unsigned int qpp){ quotaPerPrenotazione=qpp;}
void Albergo::setPrenotazioniMeseCorrente(unsigned int pmc){ prenotazioniMeseCorrente=pmc;}

