#ifndef CAMPEGGIO_H
#define CAMPEGGIO_H


#include"ImpresaRicettiva.h"

class Campeggio: public ImpresaRicettiva{
private:
                                        //CAMPI DATI

    bool bagnoPersonale;
    double costoParcheggio;
    bool tendaFornita;

    double quotaPerPersona;                                 //importo che Fatturando si trattiene per ogni persona
    unsigned int numeroOspitatiMeseCorrente;                //numero delle persone ospitate nel mese corrente

public:
                                        //COSTRUTTORE

    Campeggio(string no, string ti, string in, string ci, string na, double quo, bool ba=false, double co=0, bool ten=false,unsigned int numosp=0);
                                        //DISTRUTTORE
    ~Campeggio(){};
                                        //METODI RIDEFINITI

    void resetMese();                                       //resetta numeroOspitatiMeseCorrente

    void incrementa(unsigned int tot);                      //incrementa numeroOspitatiMeseCorrente

    double fatturatoMeseCorrente() const;                   //calcola e ritorna il fatturato del mese corrente che viene calcolato moltiplicando la quota per persona con il numero di persone

                                        //GETTERS
    double getCostoParcheggio()const;
    bool getBagnoPersonale() const;
    bool getTendaFornita() const;
    double getQuotaPerPersona() const;
    unsigned int getNumeroOspitatiMeseCorrente() const ;

                                        //SETTERS
    void setCostoParcheggio( double temp);
    void setBagnoPersonale( bool temp);
    void setTendaFornita( bool temp);
    void setQuotaPerPersona( double temp);
    void setNumeroOspitatiMeseCorrente( unsigned int temp);

};

#endif //  CAMPEGGIO_H

