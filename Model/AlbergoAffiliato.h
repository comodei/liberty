#ifndef ALBERGOAFFILIATO_H
#define ALBERGOAFFILIATO_H


#include"ImpresaRicettiva.h"
#include"Albergo.h"
#include"Investimento.h"

class AlbergoAffiliato: public Investimento, public Albergo{

private:
                                        //CAMPI DATI

    string agenziaDiGestione;

    double speseMensili;                                //spese del mese corrente
    double incassoMeseCorrente;                         //incassso del'albergo durante il mese corrente

public:
                                        //COSTRUTTORE
    AlbergoAffiliato(string no, string ti, string in, string ci, string na, unsigned int numsta, unsigned int st,unsigned int quo,
     string noFo, double inve, double per, unsigned int pre=0, bool co = false ,double spe=0, string age="", double inca=0);

                                        //DISTRUTTORE
    ~AlbergoAffiliato(){};
                                        //METODI

    bool investimentoRiguadagnato();                     //ritorna true sse il fatturato totale supera l'importo investito

                                        //METODI RIDEFINITI
    void resetMese();                                    // resetta speseMensili e richiama Albergo::reset()

    void incrementaIncasso(double incas);                //incrementa l'incasso del mese corrente

    bool ePresente(const string& parola) const;          // ridefinisce ePresente in quanto compaiono ulteriori dati alfanumerici (agenziaDiGestione e Investimento::nomeFondo)

    double fatturatoMeseCorrente() const;                //calcola e ritorna il fatturato del mese corrente. Viene calcolato prendendo la corretta percentuale dal guadagno
                                                         //totale del'albergo (tenendo conto delle spese mensili) sommandolo al guadagno calcolato come Albergo::fatturatoMeseCorrente().

                                        //GETTERS

    double getSpeseMensili() const;
    double getIncassoMeseCorrente() const;
    string getAgenziaDiGestione() const;

                                        //SETTERS
    void setSpeseMensili(double temp) ;
    void setIncassoMeseCorrente(double temp);
    void setAgenziaDiGestione(string temp);



};




#endif // ALBERGOAFFILIATO_H
