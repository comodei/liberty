#include "Campeggio.h"


//COSTRUTTORE
Campeggio::Campeggio(string no, string ti, string in, string ci, string na, double quo, bool ba, double co, bool ten,unsigned int numosp)
:ImpresaRicettiva(no, ti, in, ci, na),
bagnoPersonale(ba),
costoParcheggio(co),
tendaFornita(ten),
quotaPerPersona(quo),
numeroOspitatiMeseCorrente(numosp){}

void Campeggio::resetMese(){
    numeroOspitatiMeseCorrente=0;
}


void Campeggio::incrementa(unsigned int tot){
    numeroOspitatiMeseCorrente+=tot;
}



double Campeggio::fatturatoMeseCorrente() const{
    return numeroOspitatiMeseCorrente*quotaPerPersona;
}


//GETTERS
double Campeggio::getCostoParcheggio()const { return costoParcheggio;}
bool Campeggio::getBagnoPersonale() const{ return bagnoPersonale;}
bool Campeggio::getTendaFornita() const{ return tendaFornita;}
double Campeggio::getQuotaPerPersona() const{ return quotaPerPersona;}
unsigned int Campeggio::getNumeroOspitatiMeseCorrente() const { return numeroOspitatiMeseCorrente;}



//SETTERS
void Campeggio::setCostoParcheggio( double temp){ costoParcheggio=temp;}
void Campeggio::setBagnoPersonale( bool temp){ bagnoPersonale=temp;}
void Campeggio::setTendaFornita( bool temp){ tendaFornita=temp;}
void Campeggio::setQuotaPerPersona( double temp){ quotaPerPersona=temp;}
void Campeggio::setNumeroOspitatiMeseCorrente( unsigned int temp){ numeroOspitatiMeseCorrente=temp;}
