#include "AlbergoAffiliato.h"




//COSTRUTTORE
AlbergoAffiliato::AlbergoAffiliato(string no, string ti, string in, string ci, string na, unsigned int numsta, unsigned int st,unsigned int quo,
string noFo, double inve, double per, unsigned int pre, bool co,double spe, string age, double inca)
: Investimento(noFo, inve, per),
Albergo(no, ti, in, ci, na, numsta, st, quo, pre, co),
agenziaDiGestione(age),
speseMensili(spe),
incassoMeseCorrente(inca){}




bool AlbergoAffiliato::investimentoRiguadagnato(){
    if(fatturatoTotale()>=getInvestimento()){
        return true;
    }
    return false;
}


void AlbergoAffiliato::resetMese(){
    Albergo::resetMese();
    incassoMeseCorrente=0;
    speseMensili=0;
}

void AlbergoAffiliato::incrementaIncasso(double incas){
    incassoMeseCorrente+=incas;
}

bool AlbergoAffiliato::ePresente(const string& parola) const{
    return ImpresaRicettiva::ePresente(parola) || check(parola, getNomeFondo()) || check(parola, agenziaDiGestione);
}

double AlbergoAffiliato::fatturatoMeseCorrente() const{
    return incassoMeseCorrente*getPercentualePossesso() + getQuotaPerPrenotazione()*getPrenotazioniMeseCorrente() - speseMensili;
}



//GETTERS

double AlbergoAffiliato::getSpeseMensili() const{ return speseMensili;}
double AlbergoAffiliato::getIncassoMeseCorrente() const{ return incassoMeseCorrente;}
string AlbergoAffiliato::getAgenziaDiGestione() const{ return agenziaDiGestione;}






//SETTERS
void AlbergoAffiliato::setSpeseMensili(double temp) { speseMensili=temp;}
void AlbergoAffiliato::setIncassoMeseCorrente(double temp){incassoMeseCorrente=temp;}
void AlbergoAffiliato::setAgenziaDiGestione(string temp){agenziaDiGestione=temp;}



