#include "ImpresaRicettiva.h"



//COSTRUTTORE
ImpresaRicettiva::ImpresaRicettiva(string no, string ti, string in, string ci, string na)
:nome(no),
titolare(ti),
indirizzo(in),
citta(ci),
nazione(na),
fatturatoPassato(0)
{
    for(int i=0; i<12; i++){
        fatturatoPerMese[i]=0;
    }
}


double ImpresaRicettiva::fatturatoTotale() const{
    double tempfatturato=fatturatoPassato;
    for(int i=0; i<12; i++){
        tempfatturato+=fatturatoPerMese[i];
    }
    return tempfatturato;
}


void ImpresaRicettiva::chiudiMese(){
    shiftMese();
    setFatturatoUltimoMese(fatturatoMeseCorrente());
    resetMese();
}


void ImpresaRicettiva::shiftMese(){
    fatturatoPassato+=fatturatoPerMese[0];
    for(int i=0; i<11; i++){
        fatturatoPerMese[i]=fatturatoPerMese[i+1];
    }
}





bool ImpresaRicettiva::check(const string& daCercare, string doveCercare) {
    std::size_t trovata = doveCercare.find(daCercare);
    if (trovata != std::string::npos) return true;
    return false;
}



bool ImpresaRicettiva::ePresente(const string& parola) const{
return check ( parola, getNome()+" " +getTitolare()+" "+ getIndirizzo()+" "+ getCitta()+" "+ getNazione());
}

bool ImpresaRicettiva::operator==(const ImpresaRicettiva& ir) const{
    return (nome == ir.getNome()) && (indirizzo == ir.getIndirizzo()) && (citta == ir.getCitta());
}


//GETTERS
string ImpresaRicettiva::getNome() const{ return nome;}
string ImpresaRicettiva::getTitolare() const{ return titolare;}
string ImpresaRicettiva::getIndirizzo() const{ return indirizzo;}
string ImpresaRicettiva::getCitta() const{ return citta;}
string ImpresaRicettiva::getNazione() const{ return nazione;}
double ImpresaRicettiva::getFatturatoPassato() const {return fatturatoPassato;}
double ImpresaRicettiva::getFatturatoPerMese(unsigned int mese) const {return fatturatoPerMese[mese];}
double ImpresaRicettiva::getFatturatoUltimoMese() const {return fatturatoPerMese[11];}
string ImpresaRicettiva::getEtichetta() const{ return nome + "        " + titolare + "        " + indirizzo;}


//SETTERS
void ImpresaRicettiva::setNome(string   temp){nome=temp;}
void ImpresaRicettiva::setTitolare(string  temp){titolare=temp;}
void ImpresaRicettiva::setIndirizzo(string  temp){indirizzo=temp;}
void ImpresaRicettiva::setCitta(string  temp){citta=temp;}
void ImpresaRicettiva::setNazione(string  temp){nazione=temp;}
void ImpresaRicettiva::setFatturatoPassato(double temp){ fatturatoPassato=temp;}
void ImpresaRicettiva::setFatturatoUltimoMese(double temp){ fatturatoPerMese[11]=temp;}
void ImpresaRicettiva::setFatturatoPerMese(unsigned int mese, double importo) {fatturatoPerMese[mese]=importo;}












