#ifndef APPARTAMENTO_H
#define APPARTAMENTO_H

#include"ImpresaRicettiva.h"


class Appartamento: public ImpresaRicettiva{

private:
                                        //CAMPI DATI

    double metriQuadrati;
    unsigned int numeroMassimoPersone;
    unsigned int valutazione;

    double percentualeTassazione;               //percentuale che Fatturando trattiene dall'importo di ogni prenotazione effettuata
    double costoPrenotazioniMeseCorrente;       //somma degli importi di tutte le prenotazioni del mese corrente


public:
                                        //COSTRUTTORE

    Appartamento(string no, string ti, string in, string ci, string na, double me, unsigned int numper, unsigned int val,double perc, double costo=0);

                                        //DISTRUTTORE
    ~Appartamento(){};
                                        //METODI RIDEFINITI

    void resetMese();                           //resetta costoPrenotazioniMeseCorrente

    void incrementa(unsigned int tot);          //incrementa di tot costoPrenotazioniMeseCorrente

    double fatturatoMeseCorrente() const;       // calcola e restituisce il fatturato del mese corrente (costoPrenotazioniMeseCorrente*percentualeTassazione)

                                        //GETTERS

    double getMetriQuadrati()const ;
    unsigned int getNumeroMassimoPersone()const;
    unsigned int getValutazione()const;
    double getPercentualeTassazione() const;
    double getCostoPrenotazioniMeseCorrente() const;

                                        //SETTERS

    void setMetriQuadrati(double temp);
    void setNumeroMassimoPersone(unsigned int temp);
    void setValutazione(unsigned int temp);
    void setPercentualeTassazione(double temp);
    void setCostoPrenotazioniMeseCorrente(double temp);

};

#endif // APPARTAMENTO_H
