#ifndef IMPRESARICETTIVA_H
#define IMPRESARICETTIVA_H


#include<string>
#include<iostream>

using std::string;
using std::cout;
using std::endl;

class ImpresaRicettiva{

private:

                                        //CAMPI DATI
    string nome;
    string titolare;
    string indirizzo;
    string citta;
    string nazione;
    double fatturatoPerMese[12];        // array che memorizza separatamente il fatturato degli ultimi 12 mesi
    double fatturatoPassato;            //somma di tutto il fatturato escludendo gli ultimi 12 mesi


public:
                                        //COSTRUTTORE

    ImpresaRicettiva(string no, string ti, string in, string ci, string na);

                                        //DISTRUTTORE VIRTUALE

    virtual ~ImpresaRicettiva(){};


                                        //METODI

    double fatturatoTotale() const;     //restituisce il fatturato totale

    void shiftMese();                   //compie uno shift a sinistra di tutto l'array fatturatoPerMese per liberare l'ultima posizione

    void chiudiMese();                  //si occupa di dichiarare finito il mese


    bool static check(const string& daCercare, string cavia) ;      //controlla se una parola (daCercare) è presente in un'altra (doveCercare). Restituisce true sse è stata trovata.


    virtual bool ePresente(const string& parola) const;             // richiama la funzione check su tutti i campi dati stringhe.

                                        //METODI VIRTUALI PURI

    virtual void resetMese()=0;                                     //resetta i dati utili al conteggio del fatturato del mese corrente
    virtual void incrementa(unsigned int tot=1)=0;                  //incrementa di tot la variabile principale per definire il fatturato del mese corrente
    virtual double fatturatoMeseCorrente() const=0;                 //calcola e restituisce il valore del fatturato del mese corrente

                                        //OPERATORS

    bool operator==(const ImpresaRicettiva& ir) const;



                                        //GETTERS
    string getNome() const;
    string getTitolare() const;
    string getIndirizzo() const;
    string getCitta() const;
    string getNazione() const;
    double getFatturatoPassato() const ;
    double getFatturatoPerMese(unsigned int mese) const ;
    double getFatturatoUltimoMese() const ;                         //restituisce fatturatoPerMese[11]
    string getEtichetta() const;                                    //restituisce un insieme di dati che identificano un'impresa (nome, titolare, indirizzo)

                                        //SETTERS
    void setNome(string temp);
    void setTitolare(string   temp);
    void setIndirizzo(string  temp);
    void setCitta(string   temp);
    void setNazione(string   temp);
    void setFatturatoPassato(double temp);
    void setFatturatoUltimoMese(double temp);
    void setFatturatoPerMese(unsigned int mese, double importo) ;


};


#endif // IMPRESARICETTIVA_H
