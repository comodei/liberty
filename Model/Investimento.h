#ifndef INVESTIMENTO_H
#define INVESTIMENTO_H

#include"ImpresaRicettiva.h"

class Investimento{
private:
                                        //CAMPI DATI
    string nomeFondo;

    double investimento;                                //importo investito
    double percentualePossesso;                         //percentuale di possesso dell'albergo

protected:
                                        //COSTRUTTORE
    Investimento(string no, double in, double per);

public:
                                        //DISTRUTTORE
    virtual ~Investimento()=0;                          // è stato messo =0 per far diventare la classe astratta

                                        //METODI

                                        //GETTERS

    double getInvestimento() const;
    double getPercentualePossesso() const ;
    string getNomeFondo() const ;

                                        //SETTERS

    void setInvestimento(double temp);
    void setPercentualePossesso(double temp);
    void setNomeFondo( string temp);




};




#endif // INVESTIMENTO_H
