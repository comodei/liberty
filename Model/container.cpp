#include<iostream>
#include"container.h"
#include <ctime>
#include <cstdlib>

//CONSTRUCTORS
container::container(const Vettore<ImpresaRicettiva*>& v) : imprese(v){}
//costruttore di copia di default richiama il costruttore di copia ridefinito senza condivisione di mem. di Vettore

//All'utente viene simulato un iteratore su container, che in realtà itera sul vettore imprese
container::iterator container::begin() const{
    return imprese.first(); //conversione da puntatore a iteratore
}
container::iterator container::end() const{
    if(imprese.size() == 0) return nullptr;
    return imprese.last() + 1;
}
container::const_iterator container::cbegin() const{
    return imprese.first();
}
container::const_iterator container::cend() const{
    if(imprese.size() == 0) return nullptr;
    return imprese.last() + 1;
}

//PUBLIC METHODS
void container::clear(){
    imprese = Vettore<ImpresaRicettiva*>();
}

void container::insertItem(ImpresaRicettiva* ir){
    imprese.push_back(ir);
}

void container::removeItem(ImpresaRicettiva* ir){

    imprese.remove(ir);

}


bool container::isContained(ImpresaRicettiva *ir){
    bool found = false;
    for(Vettore<ImpresaRicettiva*>::const_iterator cit=imprese.cbegin(); cit!=imprese.cend(); ++cit){
        if(*(*cit) == *ir){
            found = true;
        }
    }
    return found;
}

//Metodo che ritorna un container contenente le imprese che soddisfano il match parziale con la stringa value
container* container::searchItems(const string& value) const{
    Vettore<ImpresaRicettiva*> results;
    for(Vettore<ImpresaRicettiva*>::const_iterator cit=imprese.cbegin(); cit!=imprese.cend(); ++cit){
        if((*cit)->ePresente(value)){
			results.push_back(*(cit));
		}
	}
    return new container(results);
}

container* container::searchAlberghi() const{
    Albergo* p;
    Vettore<ImpresaRicettiva*> results;
    for(Vettore<ImpresaRicettiva*>::const_iterator cit=imprese.cbegin(); cit!=imprese.cend(); ++cit){
        p = dynamic_cast<Albergo*>(*cit);
        if(p){
            results.push_back(p);
        }
    }

    return new container(results);
}

container* container::searchAppartamenti() const{
    Appartamento* p;
    Vettore<ImpresaRicettiva*> results;
    for(Vettore<ImpresaRicettiva*>::const_iterator cit=imprese.cbegin(); cit!=imprese.cend(); ++cit){
        p = dynamic_cast<Appartamento*>(*cit);
        if(p){
            results.push_back(p);
        }
    }

    return new container(results);
}

container* container::searchCampeggi() const{
    Campeggio* p;
    Vettore<ImpresaRicettiva*> results;
    for(Vettore<ImpresaRicettiva*>::const_iterator cit=imprese.cbegin(); cit!=imprese.cend(); ++cit){
        p = dynamic_cast<Campeggio*>(*cit);
        if(p){
            results.push_back(p);
        }
    }

    return new container(results);
}


ImpresaRicettiva* container::operator[](unsigned int i) const{
    return imprese[i];
}


//Metodo usato soltanto al fine di popolare il container 
void container::popolamento(){

    ImpresaRicettiva * impPointer;
    impPointer =new Albergo("AlbeAlbergo", "Giovanni Mucciccia", "via Sols 15", "Padova", "Italia", 25, 4, 7, 8, true);
    insertItem(impPointer);
    impPointer= new Campeggio("Scout Heaven", "Lucia Limorfi", "via Lungomare 17", "Chioggia", "Italia", 5, false, 10, true, 34);
    insertItem(impPointer);
    impPointer= new Appartamento("Longap", "Jude Son", "oxford street 28", "Londra", "Inghilterra", 45, 4, 8, 0.15, 4000);
    insertItem(impPointer);
    impPointer= new AlbergoAffiliato("pHotel", "Lana Sandes", "via Abombazza 4", "Napoli", "Italia,", 6, 1, 2, "Davide Benotti", 40000, 0.5, 100, false, 1000, "Lindo", 3000);
    insertItem(impPointer);
    impPointer= new AlbergoAffiliato("pHotel", "Lucas Shore", "via Ogni Santi 16", "Roma", "Italia,", 8, 3, 1, "Davide Benotti", 60000, 0.3, 200, true, 1000, "Pulizie", 9000);
    insertItem(impPointer);
    impPointer= new Appartamento("Apparto", "Nadia Polco", "via Talmi 1", "Venezia", "Italia", 60, 2, 10, 0.25, 500);
    insertItem(impPointer);
    impPointer= new Campeggio("Sabbia Comoda", "Matilde Randi", "via Caravaggio 10", "Firenze", "Italia", 9, true, 12, false, 24);
    insertItem(impPointer);
    impPointer =new Albergo("Olimpo", "Matilde Pi", "via della Saetta 5", "Tebe", "Grecia", 20, 2, 7, 12, false);
    insertItem(impPointer);
    impPointer= new AlbergoAffiliato("pHotel", "Vincenzo Longhi", "via Michelangelo 10", "Roma", "Italia,", 2, 6, 9, "Chiara Cocchi", 90000, 0.8, 900, true, 1000, "Pulizie", 9000);
    insertItem(impPointer);
    impPointer= new Campeggio("Tenda Lover", "Manuel Mizzo", "via Bellavista 1", "Palermo", "Italia", 10, true, 9 , true, 10);
    insertItem(impPointer);


    //Attribuiamo valori numerici randomici alle informazioni sulla contabilità delle imprese
    srand(time(NULL));

    int numMese=0;
    for(container::const_iterator cit = cbegin(); cit!= cend(); cit++){
        while(numMese<12){
               (*cit)->setFatturatoPerMese(numMese, ((rand() %1000) + 100));
                (*cit)->incrementa((rand() %100));
            numMese++;
        }
        (*cit)->setFatturatoPassato((rand() %1000));
        numMese=0;
    }
}
