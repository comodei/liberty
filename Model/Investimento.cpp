#include "Investimento.h"

//DISTRUTTORE
Investimento::~Investimento(){};


//COSTRUTTORE
Investimento::Investimento(string no, double in, double per)
:nomeFondo(no),
investimento(in),
percentualePossesso(per){}

//GETTERS

double Investimento::getInvestimento() const{ return investimento;}
double Investimento::getPercentualePossesso() const {return percentualePossesso;}
string Investimento::getNomeFondo() const {return nomeFondo;}

//SETTERS

void Investimento::setInvestimento(double temp) {investimento=temp;}
void Investimento::setPercentualePossesso(double temp) {percentualePossesso=temp;}
void Investimento::setNomeFondo( string temp) { nomeFondo=temp;}



