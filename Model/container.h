#ifndef CONTAINER_H
#define CONTAINER_H

#include<iostream>
#include"vettore.h"
#include"ImpresaRicettiva.h"
#include"Albergo.h"
#include"Appartamento.h"
#include"Campeggio.h"
#include"AlbergoAffiliato.h"

using std::string;


class container{

    private:

        Vettore<ImpresaRicettiva*> imprese;
    
    public:

        //CONSTRUCTORS
        container(const Vettore<ImpresaRicettiva*>& v = Vettore<ImpresaRicettiva*>());
        //costruttore di copia di default richiama il costruttore di copia ridefinito senza cond. mem. di Vettore

        //ITERATORS
        typedef Vettore<ImpresaRicettiva*>::iterator iterator;
        typedef Vettore<ImpresaRicettiva*>::const_iterator const_iterator;

        iterator begin() const;
        iterator end() const;
        const_iterator cbegin() const;
        const_iterator cend() const;

        //PUBLIC METHODS
        void clear();
        void insertItem(ImpresaRicettiva* ir);
        void removeItem(ImpresaRicettiva* ir);
        bool isContained(ImpresaRicettiva* ir);
        container* searchItems(const string& value) const;
        container* searchAlberghi() const;
        container* searchAppartamenti() const;
        container* searchCampeggi() const;

        void popolamento();                         //il container viene popolato per simulare l'inserimento da file. I dati numerici sono calcolati randomicamente.

        //OPERATORS
        ImpresaRicettiva* operator[](unsigned int i) const;


};


#endif
