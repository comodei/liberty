#include "Appartamento.h"



Appartamento::Appartamento(string no, string ti, string in, string ci, string na, double me, unsigned int numper, unsigned int val,double perc, double costo)
:ImpresaRicettiva(no, ti, in, ci, na),
metriQuadrati(me),
numeroMassimoPersone(numper),
valutazione(val),
percentualeTassazione(perc),
costoPrenotazioniMeseCorrente(costo){}



void Appartamento::resetMese(){
    costoPrenotazioniMeseCorrente=0;
}

void Appartamento::incrementa(unsigned int tot){
    costoPrenotazioniMeseCorrente+=tot;
}


double Appartamento::fatturatoMeseCorrente() const{
    return costoPrenotazioniMeseCorrente*percentualeTassazione/100;
}

//GETTERS

double  Appartamento::getMetriQuadrati()const { return metriQuadrati;}
unsigned int  Appartamento::getNumeroMassimoPersone()const{ return numeroMassimoPersone;}
unsigned int  Appartamento::getValutazione()const{ return valutazione;}
double  Appartamento::getPercentualeTassazione() const{ return percentualeTassazione;}
double  Appartamento::getCostoPrenotazioniMeseCorrente() const{ return costoPrenotazioniMeseCorrente;}




//SETTERS

void Appartamento::setMetriQuadrati(double temp){ metriQuadrati=temp;}
void Appartamento::setNumeroMassimoPersone(unsigned int temp){ numeroMassimoPersone=temp;}
void Appartamento::setValutazione(unsigned int temp){ valutazione=temp;}
void Appartamento::setPercentualeTassazione(double temp){ percentualeTassazione=temp;}
void Appartamento::setCostoPrenotazioniMeseCorrente(double temp){ costoPrenotazioniMeseCorrente=temp;}



