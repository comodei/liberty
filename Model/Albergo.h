#ifndef ALBERGO_H
#define ALBERGO_H


#include"ImpresaRicettiva.h"


class Albergo: public ImpresaRicettiva{

private:
                                        //CAMPI DATI
    unsigned int numeroStanze;
    unsigned int stelle;
    bool colazione;

    unsigned int quotaPerPrenotazione;              //memorizza la quota che Fatturando trattiene per ogni prenotazione effettuata
    unsigned int prenotazioniMeseCorrente;          //il numero di prenotazioni effettutate nel mese corrente


public:
                                        //COSTRUTTORE
    Albergo(string no, string ti, string in, string ci, string na, unsigned int numsta, unsigned int st, unsigned int quo, unsigned int pre=0, bool co = false);

                                        //DISTRUTTORE
    ~Albergo(){};

                                        //MEDOTI RIDEFINITI

    void resetMese();                               //resetta prenotazioniMeseCorrente

    void incrementa(unsigned int tot=1);            //incrementa prenotazioniMeseCorrente

    double fatturatoMeseCorrente() const;           //calcola e ritorna il fatturato del mese corrente (quota per prenotazione * prenotazioni del mese)


                                        //GETTERS

    unsigned int getQuotaPerPrenotazione() const;
    unsigned int getPrenotazioniMeseCorrente() const;
    unsigned int getNumeroStanze() const;
    unsigned int getStelle() const;
    bool getColazione() const;

                                        //SETTERS

    void setNumeroStanze(unsigned int temp);
    void setStelle(unsigned int temp);
    void setColazione(bool temp);
    void setQuotaPerPrenotazione(unsigned int qpp);
    void setPrenotazioniMeseCorrente(unsigned int pmc);


};


#endif // ALBERGO_H
