#include"aggiuntawindow.h"
#include<QToolBox>
#include<QMessageBox>



AggiuntaWindow::AggiuntaWindow(container* c, QWidget* parent): QWidget(parent), contenitore(c){

    setWindowTitle("Aggiunta Impresa");

    glayout = new QGridLayout;
    QLabel* tipo = new QLabel("Inserire tipologia impresa:");
    tipoImpresa = new QComboBox();
    tipoImpresa->addItem(" ");
    tipoImpresa->addItem("Albergo");
    tipoImpresa->addItem("Appartamento");
    tipoImpresa->addItem("Campeggio");
    tipoImpresa->addItem("Albergo Affiliato");
    aggiunta = new QPushButton(tr("Aggiungi Impresa"));
    aggiunta->setEnabled(false);

    QToolBox* FrameImpresa = new QToolBox();
    QWidget* toFrame = new QWidget();
    QGridLayout* frameLayout = new QGridLayout;
    Nome = new QLabel("*Nome:");
    Titolare = new QLabel("*Titolare:");
    Nazione = new QLabel("Nazione:");
    Indirizzo = new QLabel("*Indirizzo:");
    Citta = new QLabel("Città:");
    EditNome = new QLineEdit();
    EditTitolare = new QLineEdit();
    EditNazione = new QLineEdit();
    EditIndirizzo = new QLineEdit();
    EditCitta = new QLineEdit();
    QLabel* info = new QLabel("*campi obbligatori");
    frameLayout->addWidget(Nome,0,0,1,1);
    frameLayout->addWidget(EditNome,0,1,1,4);
    frameLayout->addWidget(Titolare,0,5,1,1);
    frameLayout->addWidget(EditTitolare,0,6,1,4);
    frameLayout->addWidget(Indirizzo,1,0,1,1);
    frameLayout->addWidget(EditIndirizzo,1,1,1,4);
    frameLayout->addWidget(Citta,1,5,1,1);
    frameLayout->addWidget(EditCitta,1,6,1,4);
    frameLayout->addWidget(Nazione,2,0,1,1);
    frameLayout->addWidget(EditNazione,2,1,1,4);
    frameLayout->addWidget(info,3,0,1,1);
    toFrame->setLayout(frameLayout);
    FrameImpresa->addItem(toFrame,"Informazioni generali:");
    FrameImpresa->setFrameStyle(QFrame::Box | QFrame::Plain);
    FrameImpresa->setLineWidth(1);
    FrameImpresa->setMinimumSize(550,200);

    glayout->addWidget(tipo,0,0,1,1);
    glayout->addWidget(tipoImpresa,0,1,1,10);
    glayout->addWidget(FrameImpresa,1,0,1,10);
    glayout->addWidget(aggiunta,3,3,1,2);
    setLayout(glayout);

    connect(tipoImpresa, SIGNAL(currentTextChanged(const QString&)), this, SLOT(showInfoSpecifiche(const QString&)));
    connect(aggiunta, SIGNAL(clicked()), this, SLOT(buildImpresa()));

}

void AggiuntaWindow::showInfoSpecifiche(const QString& text){

    tipoImpresa->setEnabled(false);
    QToolBox* toAdd = new QToolBox();
    QWidget* frame = new QWidget();
    QGridLayout* frameLayout = new QGridLayout;

    if(text == "Albergo"){

        QLabel* stanze = new QLabel("Num. stanze:");
        num_stanze = new QSpinBox();
        num_stanze->setRange(0,999);
        QLabel* stelle = new QLabel("Num. stelle:");
        num_stelle = new QSpinBox();
        num_stelle->setRange(1,5);
        QLabel* colazione = new QLabel("Colazione:");
        haveColazione = new QComboBox();
        haveColazione->addItem("No");
        haveColazione->addItem("Si");
        QLabel* quota = new QLabel("Quota per prenotazione:");
        num_quota = new QSpinBox();
        num_quota->setRange(0,9999);
        QLabel* prenotazioni = new QLabel("Prenotazioni mese corrente");
        num_prenotazioni = new QSpinBox();
        num_prenotazioni->setRange(0,999);

        frameLayout->addWidget(stanze,0,0,1,1);
        frameLayout->addWidget(num_stanze,0,1,1,4);
        frameLayout->addWidget(stelle,0,5,1,1);
        frameLayout->addWidget(num_stelle,0,6,1,4);
        frameLayout->addWidget(colazione,1,0,1,1);
        frameLayout->addWidget(haveColazione,1,1,1,4);
        frameLayout->addWidget(quota,1,5,1,1);
        frameLayout->addWidget(num_quota,1,6,1,4);
        frameLayout->addWidget(prenotazioni,2,0,1,1);
        frameLayout->addWidget(num_prenotazioni,2,1,1,4);
        toAdd->setMinimumSize(550,200);

    }

    else if(text == "Appartamento"){

        QLabel* metri = new QLabel("mq.:");
        num_metri = new QDoubleSpinBox();
        num_metri->setRange(00.00,999.99);
        num_metri->setSingleStep(0.25);
        QLabel* persone = new QLabel("Numero max persone:");
        num_persone =  new QSpinBox();
        num_persone->setRange(1,10);
        QLabel* valutazione = new QLabel("Valutazione:");
        num_valutazione = new QSpinBox();
        num_valutazione->setRange(1,10);
        QLabel* tassazione = new QLabel("Tassazione[%]:");
        num_tassazione = new QDoubleSpinBox();
        num_tassazione->setSingleStep(0.1);
        num_tassazione->setRange(0.0,100.0);
        QLabel* costoPrenotazioni = new QLabel("Costo prenotazioni[€]:");
        num_costo = new QDoubleSpinBox();
        num_costo->setRange(0.0,9999.99);

        frameLayout->addWidget(metri,0,0,1,1);
        frameLayout->addWidget(num_metri,0,1,1,4);
        frameLayout->addWidget(persone,0,5,1,1);
        frameLayout->addWidget(num_persone,0,6,1,4);
        frameLayout->addWidget(valutazione,1,0,1,1);
        frameLayout->addWidget(num_valutazione,1,1,1,4);
        frameLayout->addWidget(costoPrenotazioni,1,5,1,1);
        frameLayout->addWidget(num_costo,1,6,1,4);
        frameLayout->addWidget(tassazione,2,0,1,1);
        frameLayout->addWidget(num_tassazione,2,1,1,4);
        toAdd->setMinimumSize(550,200);

    }

    else if(text == "Campeggio"){
        QLabel* bagno = new QLabel("Bagno:");
        haveBagno = new QComboBox();
        haveBagno->addItem("No");
        haveBagno->addItem("Si");
        QLabel* tenda = new QLabel("Tenda fornita:");
        haveTenda = new QComboBox();
        haveTenda->addItem("No");
        haveTenda->addItem("Si");
        QLabel* parcheggio = new QLabel("Costo Parcheggio[€]:");
        costo_parcheggio = new QDoubleSpinBox();
        costo_parcheggio->setRange(0.00,999.99);
        costo_parcheggio->setSingleStep(0.50);
        QLabel* quotaPersona = new QLabel("Quota Per Persona[€]:");
        costo_persona = new QDoubleSpinBox();
        costo_persona->setRange(0.00,999.99);
        costo_persona->setSingleStep(0.50);
        QLabel* ospitanti = new QLabel("Numero ospitanti mese:");
        num_ospitanti = new QSpinBox();
        num_ospitanti->setRange(0,999);

        frameLayout->addWidget(bagno,0,0,1,1);
        frameLayout->addWidget(haveBagno,0,1,1,4);
        frameLayout->addWidget(tenda,0,5,1,1);
        frameLayout->addWidget(haveTenda,0,6,1,4);
        frameLayout->addWidget(parcheggio,1,0,1,1);
        frameLayout->addWidget(costo_parcheggio,1,1,1,4);
        frameLayout->addWidget(quotaPersona,1,5,1,1);
        frameLayout->addWidget(costo_persona,1,6,1,4);
        frameLayout->addWidget(ospitanti,2,0,1,1);
        frameLayout->addWidget(num_ospitanti,2,1,1,4);
        toAdd->setMinimumSize(550,200);
    }

    else if(text == "Albergo Affiliato"){

        QLabel* stanze = new QLabel("Num. stanze:");
        num_stanze = new QSpinBox();
        num_stanze->setRange(0,999);
        QLabel* stelle = new QLabel("Num. stelle:");
        num_stelle = new QSpinBox();
        num_stelle->setRange(1,5);
        QLabel* colazione = new QLabel("Colazione:");
       haveColazione = new QComboBox();
        haveColazione->addItem("No");
        haveColazione->addItem("Si");
        QLabel* quota = new QLabel("Quota per prenotazione:");
        num_quota = new QSpinBox();
        num_quota->setRange(0,9999);
        QLabel* prenotazioni = new QLabel("Prenotazioni mese corrente");
        num_prenotazioni = new QSpinBox();
        num_prenotazioni->setRange(0,999);
        QLabel* fondo = new QLabel("Nome fondo:");
        nome_fondo = new QLineEdit();
        QLabel* investimento = new QLabel("Investimento[€]:");
        num_investimento = new QDoubleSpinBox();
        num_investimento->setRange(0.00,999999.99);
        num_investimento->setSingleStep(0.50);
        QLabel* possesso = new QLabel("Possesso[%]:");
        num_possesso = new QDoubleSpinBox();
        num_possesso->setRange(0.0,100.0);
        num_possesso->setSingleStep(0.1);
        QLabel* spese = new QLabel("Spese mensili[€]:");
        num_spese = new QDoubleSpinBox();
        num_spese->setRange(0.00,999999.99);
        QLabel* agenzia = new QLabel("Agenzia di gestione:");
        nome_agenzia = new QLineEdit();
        QLabel* incasso = new QLabel("Incasso mese[€]:");
        num_incasso = new QDoubleSpinBox();
        num_incasso->setRange(0.00,999999.99);
        num_incasso->setSingleStep(0.50);

        frameLayout->addWidget(stanze,0,0,1,1);
        frameLayout->addWidget(num_stanze,0,1,1,4);
        frameLayout->addWidget(stelle,0,5,1,1);
        frameLayout->addWidget(num_stelle,0,6,1,4);
        frameLayout->addWidget(colazione,1,0,1,1);
        frameLayout->addWidget(haveColazione,1,1,1,4);
        frameLayout->addWidget(quota,1,5,1,1);
        frameLayout->addWidget(num_quota,1,6,1,4);
        frameLayout->addWidget(prenotazioni,2,0,1,1);
        frameLayout->addWidget(num_prenotazioni,2,1,1,4);
        frameLayout->addWidget(fondo,2,5,1,1);
        frameLayout->addWidget(nome_fondo,2,6,1,4);
        frameLayout->addWidget(investimento,3,0,1,1);
        frameLayout->addWidget(num_investimento,3,1,1,4);
        frameLayout->addWidget(possesso,3,5,1,1);
        frameLayout->addWidget(num_possesso,3,6,1,4);
        frameLayout->addWidget(spese,4,0,1,1);
        frameLayout->addWidget(num_spese,4,1,1,4);
        frameLayout->addWidget(agenzia,4,5,1,1);
        frameLayout->addWidget(nome_agenzia,4,6,1,4);
        frameLayout->addWidget(incasso,5,0,1,1);
        frameLayout->addWidget(num_incasso,5,1,1,4);
        toAdd->setMinimumSize(650,400);


    }

    frame->setLayout(frameLayout);
    toAdd->addItem(frame,"Informazioni specifiche:");
    aggiunta->setEnabled(true);
    glayout->addWidget(toAdd,2,0,1,10);

}

void AggiuntaWindow::buildImpresa(){

    ImpresaRicettiva* ir = nullptr;

    if(EditNome->text()==""||EditTitolare->text()==""||EditIndirizzo->text()==""){
        QMessageBox* msg = new QMessageBox();
        msg->setIcon(QMessageBox::Critical);
        msg->setWindowTitle("Attenzione");
        msg->setText("Inserire tutti i campi obbligatori");
        msg->exec();
    }
    else{

        switch(tipoImpresa->currentIndex()){

        case 1:
            ir = new Albergo(EditNome->text().toStdString(),EditTitolare->text().toStdString()
                             ,EditIndirizzo->text().toStdString(),EditCitta->text().toStdString()
                             ,EditNazione->text().toStdString(),num_stanze->value(),num_stelle->value()
                             ,num_quota->value(),num_prenotazioni->value(),haveColazione->currentIndex());
            break;

        case 2:

            ir = new Appartamento(EditNome->text().toStdString(),EditTitolare->text().toStdString()
                                  ,EditIndirizzo->text().toStdString(),EditCitta->text().toStdString()
                                  ,EditNazione->text().toStdString(),num_metri->value(),num_persone->value()
                                  ,num_valutazione->value(),num_tassazione->value());
            break;

        case 3:

            ir = new Campeggio(EditNome->text().toStdString(),EditTitolare->text().toStdString()
                               ,EditIndirizzo->text().toStdString(),EditCitta->text().toStdString()
                               ,EditNazione->text().toStdString(),costo_persona->value(),haveBagno->currentIndex()
                               ,costo_parcheggio->value(),haveTenda->currentIndex(),num_ospitanti->value());
            break;

        case 4:

            ir = new AlbergoAffiliato(EditNome->text().toStdString(),EditTitolare->text().toStdString()
                                      ,EditIndirizzo->text().toStdString(),EditCitta->text().toStdString()
                                      ,EditNazione->text().toStdString(),num_stanze->value(),num_stelle->value()
                                      ,num_quota->value(),nome_fondo->text().toStdString(),num_investimento->value()
                                      ,num_possesso->value(),num_prenotazioni->value(),haveColazione->currentIndex()
                                      ,num_spese->value(),nome_agenzia->text().toStdString(),num_incasso->value());
            break;
        }

        contenitore->insertItem(ir);
        QMessageBox* msg = new QMessageBox();
        msg->setIcon(QMessageBox::Information);
        msg->setWindowTitle("Informazione");
        msg->setText("Impresa inserita correttamente");
        msg->exec();

        emit listaModificata();
        chiudiFinestra();
    }
}

void AggiuntaWindow::chiudiFinestra(){
    delete this;
}
AggiuntaWindow::~AggiuntaWindow(){

}
