#include "mainwindow.h"
#include<QString>

MainWindow::MainWindow(container* c, QWidget *parent): QMainWindow(parent),  display(c), backup(c){

    setWindowTitle("Prenotando");
    setMinimumSize(QSize(800,500));

    centralWidget = new QWidget(this);
    setCentralWidget(centralWidget);
    layout = new QGridLayout(centralWidget);

    keyword_ricerca = new QLineEdit();
    ricerca = new QToolButton();
    ricerca->setIcon(QIcon(":/images/lente.png"));
    aggiungi = new QPushButton(tr("Aggiungi Impresa"));
    fatturato = new QPushButton(tr("Gestisci fatturato"));
    fatturato->setToolTip("Gestisci il fatturato delle imprese mostrate");
    fatturato->setToolTipDuration(2000);
    HotelFilter = new QRadioButton(tr("Alberghi"));
    HotelFilter->setCheckable(true);
    AppFilter = new QRadioButton(tr("Appartamenti"));
    CampFilter = new QRadioButton(tr("Campeggi"));
    RadioBox = new QGroupBox("Filtri:");
    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addWidget(HotelFilter);
    vbox->addWidget(AppFilter);
    vbox->addWidget(CampFilter);
    RadioBox->setLayout(vbox);
    resetFilters = new QPushButton(tr("Rimuovi filtri"));

    logoHomePage= new QPushButton();
    logoHomePage->setIcon(QIcon((":/images/logo.png")));
    logoHomePage->setIconSize(QSize(120, 50));
    logoHomePage->setFixedSize(QSize(120, 50));
    logoHomePage->setToolTip("Clicca per fare il refresh");
    logoHomePage->setToolTipDuration(2000);


    sc_area = new QScrollArea();
    lista = new QListWidget(sc_area);
    lista->setToolTip("Doppio click per visualizzare, modificare o rimuovere l'impresa");
    lista->setToolTipDuration(2000);
    sc_area->setWidgetResizable(true);
    QGridLayout* glayout = new QGridLayout(sc_area);
    glayout->addWidget(lista,0,0,1,5);
    sc_area->setWidget(lista);


    layout->addWidget(logoHomePage,0,0,1, 1);
    layout->addWidget(keyword_ricerca,0,1,1,2);
    layout->addWidget(ricerca,0,3,1,1);
    layout->addWidget(aggiungi,0,4,1,1);
    layout->addWidget(fatturato,0,5,1,1);
    layout->addWidget(sc_area,1,0,10,6);
    layout->addWidget(RadioBox,1,6,1,1);
    layout->addWidget(resetFilters,2,6,1,1);

    centralWidget->setLayout(layout);



    //LISTA
    showList();
    connect(keyword_ricerca, SIGNAL(textChanged(const QString&)), this, SLOT(showSearchList(const QString&)));      //quando il testo all'interno della barra di ricerca cambia viene effettuata la ricerca e messo a display il risultato
    connect(logoHomePage, SIGNAL(clicked()), this, SLOT(refresh()));                    //se viene premuto il logo si chiama refresh()
    connect(logoHomePage, SIGNAL(clicked()), keyword_ricerca, SLOT(clear()));           //quando viene premuto il logo viene pulita la barra di ricerca
    connect(HotelFilter, SIGNAL(toggled(bool)), this, SLOT(showAlberghi(bool)));        //quando viene premuto un filtro si mettono a display solo i tipi selezionati
    connect(AppFilter, SIGNAL(toggled(bool)), this, SLOT(showAppartamenti(bool)));
    connect(CampFilter, SIGNAL(toggled(bool)), this, SLOT(showCampeggi(bool)));
    connect(fatturato, SIGNAL(clicked()), this, SLOT(creaFinestraContabilita()));       //quando viene premuto il pulsante fatturato si apre la finestra contabilitawindow
    connect(aggiungi, SIGNAL(clicked()), this, SLOT(creaFinestraAggiunta()));           //quando viene premuto il pulsante aggiungi si apre la finestra aggiuntawindow
    connect(lista, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(creaFinestraModifica(QListWidgetItem*)));     //quando si fa doppio click su una impresa si apre la finestra modificawindowcon quella impresa come parametro
    connect(resetFilters,SIGNAL(clicked()), this, SLOT(refresh()));        //se premuto il pulsante resetFilters vengono eliminati i filtri



}

MainWindow::~MainWindow()
{
}

void MainWindow::clearFilters() {
    HotelFilter->setAutoExclusive(false);
    AppFilter->setAutoExclusive(false);
    CampFilter->setAutoExclusive(false);
    HotelFilter->setChecked(false);
    AppFilter->setChecked(false);
    CampFilter->setChecked(false);
    HotelFilter->setAutoExclusive(true);
    AppFilter->setAutoExclusive(true);
    CampFilter->setAutoExclusive(true);
}

void MainWindow::showList(){

    lista->clear();

    for(container::const_iterator cit = display->cbegin(); cit!= display->cend(); cit++){
        lista->addItem(new QListWidgetItem(QString::fromStdString((*cit)->getEtichetta())));
    }
}
void MainWindow::showSearchList(const QString& text){

    if(text.isEmpty()){
        refresh();
    }
    else{
        display = backup->searchItems(text.toStdString());
        showList();
    }
}

void MainWindow::refresh(){
    clearFilters();
    display = backup;
    showList();
}

void MainWindow::showAlberghi(bool checked){
    if(checked){
        display = backup->searchAlberghi();
        showList();
    }
}

void MainWindow::showCampeggi(bool checked){
    if(checked){
        display = backup->searchCampeggi();
        showList();
    }
}

void MainWindow::showAppartamenti(bool checked){
    if(checked){
        display = backup->searchAppartamenti();
        showList();
    }
}


void MainWindow::creaFinestraContabilita() const{
    ContabilitaWindow* finestra= new ContabilitaWindow(display);
    finestra->show();
}

void MainWindow::creaFinestraAggiunta() const {
    AggiuntaWindow* finestra = new AggiuntaWindow(backup);
    finestra->show();
    connect(finestra,SIGNAL(listaModificata()), this, SLOT(refresh()));
}

void MainWindow::creaFinestraModifica(QListWidgetItem* item) const{

    QString text = item->text();
    for(container::const_iterator cit=display->cbegin(); cit!=display->cend(); ++cit){
        if((*cit)->getEtichetta() == text.toStdString()){
            ModificaWindow* finestra = new ModificaWindow(*cit,backup);
            finestra->show();
            connect(finestra,SIGNAL(listaModificata()), this, SLOT(refresh()));
            break;
        }

    }
}
