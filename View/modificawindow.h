#ifndef MODIFICAWINDOW_H
#define MODIFICAWINDOW_H


#include<QWidget>
#include<QComboBox>
#include<QSpinBox>
#include<QLabel>
#include<QLineEdit>
#include<QGridLayout>
#include<QPushButton>
#include<./Model/ImpresaRicettiva.h>
#include<./Model/Investimento.h>
#include<./Model/container.h>
#include<./Model/Albergo.h>
#include<./Model/Appartamento.h>
#include<./Model/Campeggio.h>
#include<./Model/AlbergoAffiliato.h>
#include "View/mainwindow.h"

class ModificaWindow: public QWidget{
    Q_OBJECT

private:

    ImpresaRicettiva* impresa;          //impresa in esame
    container* backup;                  // container contenente tutte le imprese

    QComboBox   *tipoImpresa,
                *haveColazione,
                *haveBagno,
                *haveTenda;

    QLabel  *Nome,
            *Titolare,
            *Indirizzo,
            *Citta,
            *Nazione;

    QLineEdit   *EditNome,
                *EditTitolare,
                *EditIndirizzo,
                *EditCitta,
                *EditNazione,
                *nome_fondo,
                *nome_agenzia;

    QSpinBox    *num_stanze,
                *num_stelle,
                *num_quota,
                *num_prenotazioni,
                *num_persone,
                *num_valutazione,
                *num_ospitanti;

    QDoubleSpinBox  *num_metri,
                    *num_tassazione,
                    *num_costo,
                    *costo_parcheggio,
                    *costo_persona,
                    *num_investimento,
                    *num_possesso,
                    *num_spese,
                    *num_incasso;

    QGridLayout* glayout;
    QPushButton* modifica;
    QPushButton* elimina;

    void chiudiFinestra();
    void showInfoSpecifiche();          //effettua il display delle informazioni specifiche in base al tipo dinamico dell'impresa in esame


public:
    ModificaWindow(ImpresaRicettiva* ir, container* bac,QWidget *parent = nullptr);
    ~ModificaWindow();

signals:
    void listaModificata();

private slots:

    void mod();                         //effettua la modifica dei dati
    void eliminaImpresa();              //elimina l'impresa in esame e la rimuove dal container

};

#endif // MODIFICAWINDOW_H
