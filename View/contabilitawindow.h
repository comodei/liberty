#ifndef CONTABILITAWINDOW_H
#define CONTABILITAWINDOW_H

#include <QWidget>
#include<QPushButton>
#include<QToolButton>
#include<QGridLayout>
#include<QLabel>
#include<QTableWidget>
#include<QLineEdit>
#include<QMessageBox>
#include"./Model/container.h"


class ContabilitaWindow : public QWidget{

    Q_OBJECT

private:

    container* conte;               //il container contenente l'elenco delle imprese su cui si vuole agire
    QGridLayout* layout;
    QPushButton* chiudiMese;
    QLineEdit *TotFatturato,
              *TotMensile;
    QTableWidget* tabella;


    double getFatturatoTotale() const;                      //calcola la somma di tutti i fatturati totali
    double getFatturatoMensile() const;                     //calcola la somma di tutti i fatturati mensili
    QString getFatturatoPerMese(int mese) const;            //calcola la somma mese per mese di tutti i fattuati mensili
    void impostaDati();
    void riempiTabella();


public:

    ContabilitaWindow(container* c, QWidget *parent = nullptr);
    ~ContabilitaWindow();



private slots:

    void chiudiTuttiMesi();

};

#endif // CONTABILITAWINDOW_H
