#ifndef AGGIUNTAWINDOW_H
#define AGGIUNTAWINDOW_H

#include<QWidget>
#include<QComboBox>
#include<QSpinBox>
#include<QLabel>
#include<QLineEdit>
#include<QGridLayout>
#include<QPushButton>
#include<./Model/container.h>
#include<./Model/Albergo.h>
#include<./Model/Appartamento.h>
#include<./Model/Campeggio.h>
#include<./Model/AlbergoAffiliato.h>

class AggiuntaWindow : public QWidget{
    Q_OBJECT

private:

    container* contenitore;

    QComboBox   *tipoImpresa,
                *haveColazione,
                *haveBagno,
                *haveTenda;

    QLabel  *Nome,
            *Titolare,
            *Indirizzo,
            *Citta,
            *Nazione;

    QLineEdit   *EditNome,
                *EditTitolare,
                *EditIndirizzo,
                *EditCitta,
                *EditNazione,
                *nome_fondo,
                *nome_agenzia;

    QSpinBox    *num_stanze,
                *num_stelle,
                *num_quota,
                *num_prenotazioni,
                *num_persone,
                *num_valutazione,
                *num_ospitanti;

    QDoubleSpinBox  *num_metri,
                    *num_tassazione,
                    *num_costo,
                    *costo_parcheggio,
                    *costo_persona,
                    *num_investimento,
                    *num_possesso,
                    *num_spese,
                    *num_incasso;

    QGridLayout* glayout;
    QPushButton* aggiunta;
    
    void chiudiFinestra();

public:
    AggiuntaWindow(container*, QWidget* parent = nullptr);
    ~AggiuntaWindow();

signals:
    void listaModificata();

private slots:
    void showInfoSpecifiche(const QString&);
    void buildImpresa();

};

#endif // AGGIUNTAWINDOW_H
