#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include<QMainWindow>
#include<QPushButton>
#include<QToolButton>
#include<QLineEdit>
#include<QScrollArea>
#include<QListWidget>
#include<QGridLayout>
#include<QHBoxLayout>
#include<QIcon>
#include<QPixmap>
#include<QGroupBox>
#include<QRadioButton>
#include<QLabel>
#include"./Model/container.h"
#include "View/contabilitawindow.h"
#include"View/aggiuntawindow.h"
#include"View/modificawindow.h"

class MainWindow : public QMainWindow{
    Q_OBJECT

private:

    container* display,                     //vengono utilizzati due container di imprese ricettive. display memorizza l'insieme delle imprese dopo aver effettuato una ricerca o un filtraggio
                *backup;                    //backup è il container con tutte le imprese
    QWidget* centralWidget;                 //widget principale
    QGridLayout* layout;
    QScrollArea* sc_area;
    QLineEdit* keyword_ricerca;
    QToolButton* ricerca;
    QPushButton* aggiungi,
               * fatturato,
               * logoHomePage,
               * resetFilters;
    QGroupBox* RadioBox;
    QRadioButton* HotelFilter,
                * AppFilter,
                * CampFilter;
    QListWidget* lista;

public:
    MainWindow(container* c, QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void listaModificata();

private slots:

    void clearFilters();                                //rimuove la selezione di un qualsiasi filtro
    void refresh();                                     //ripristina display usando backup
    void showList();                                    //mostra il contaier display
    void showSearchList(const QString&);
    void showAlberghi(bool);                            //mostra tutti gli alberghi presenti in display
    void showCampeggi(bool);                            //mostra tutti i campeggi presenti in display
    void showAppartamenti(bool);                        //mostra tutti gli appartamenti presenti in display

                                                        //slots per creare le finestre secondarie
    void creaFinestraContabilita() const;
    void creaFinestraAggiunta() const;
    void creaFinestraModifica(QListWidgetItem*) const;


};
#endif
