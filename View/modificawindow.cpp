#include "modificawindow.h"
#include<QToolBox>
#include<QMessageBox>

ModificaWindow::ModificaWindow(ImpresaRicettiva* ir, container* bac, QWidget* parent):QWidget(parent), impresa(ir), backup(bac){

    setWindowTitle("Modifica Impresa");
    glayout = new QGridLayout;
    modifica = new QPushButton(tr("Modifica Impresa"));
    elimina = new QPushButton(tr("Elimina Impresa"));

    QToolBox* FrameImpresa = new QToolBox();
    QWidget* toFrame = new QWidget();
    QGridLayout* frameLayout = new QGridLayout;
    Nome = new QLabel("Nome:");
    Titolare = new QLabel("Titolare:");
    Nazione = new QLabel("Nazione:");
    Indirizzo = new QLabel("Indirizzo:");
    Citta = new QLabel("Città:");
    EditNome = new QLineEdit();
    EditNome->setText(QString::fromStdString(impresa->getNome()));
    EditTitolare = new QLineEdit();
    EditTitolare->setText(QString::fromStdString(impresa->getTitolare()));
    EditNazione = new QLineEdit();
    EditNazione->setText(QString::fromStdString(impresa->getNazione()));
    EditIndirizzo = new QLineEdit();
    EditIndirizzo->setText(QString::fromStdString(impresa->getIndirizzo()));
    EditCitta = new QLineEdit();
    EditCitta->setText(QString::fromStdString(impresa->getCitta()));
    frameLayout->addWidget(Nome,0,0,1,1);
    frameLayout->addWidget(EditNome,0,1,1,4);
    frameLayout->addWidget(Titolare,0,5,1,1);
    frameLayout->addWidget(EditTitolare,0,6,1,4);
    frameLayout->addWidget(Nazione,1,0,1,1);
    frameLayout->addWidget(EditNazione,1,1,1,4);
    frameLayout->addWidget(Citta,1,5,1,1);
    frameLayout->addWidget(EditCitta,1,6,1,4);
    frameLayout->addWidget(Indirizzo,2,0,1,1);
    frameLayout->addWidget(EditIndirizzo,2,1,1,4);
    toFrame->setLayout(frameLayout);
    FrameImpresa->addItem(toFrame,"Informazioni generali:");
    FrameImpresa->setFrameStyle(QFrame::Box | QFrame::Plain);
    FrameImpresa->setLineWidth(1);
    FrameImpresa->setMinimumSize(550,200);

    glayout->addWidget(FrameImpresa,1,0,1,10);
    glayout->addWidget(modifica,3,3,1,2);
    glayout->addWidget(elimina,3,5,1,2);
    setLayout(glayout);

    showInfoSpecifiche();


    connect(modifica, SIGNAL(clicked()), this, SLOT(mod()));                //se premuto il pulsante modifica richiamama mod()
    connect(elimina, SIGNAL(clicked()), this, SLOT(eliminaImpresa()));      //se premuto il pulsante elimina richiama eliminaImpresa()
}




void ModificaWindow::showInfoSpecifiche(){



    QToolBox* toAdd = new QToolBox();
    QWidget* frame = new QWidget();
    QGridLayout* frameLay = new QGridLayout;

    if(AlbergoAffiliato* tipoAlbAff= dynamic_cast<AlbergoAffiliato*>(impresa)){

        QLabel* stanze = new QLabel("Num. stanze:");
        num_stanze = new QSpinBox();
        num_stanze->setRange(0,999);
        num_stanze->setValue(tipoAlbAff->getNumeroStanze());
        QLabel* stelle = new QLabel("Num. stelle:");
        num_stelle = new QSpinBox();
        num_stelle->setValue(tipoAlbAff->getStelle());
        num_stelle->setRange(1,5);
        QLabel* colazione = new QLabel("Colazione:");
        haveColazione = new QComboBox();
        haveColazione->addItem("No");
        haveColazione->addItem("Si");
        haveColazione->setCurrentIndex(tipoAlbAff->getColazione());
        QLabel* quota = new QLabel("Quota per prenotazione:");
        num_quota = new QSpinBox();
        num_quota->setRange(0,9999);
        num_quota->setValue(tipoAlbAff->getQuotaPerPrenotazione());
        QLabel* prenotazioni = new QLabel("Prenotazioni mese corrente");
        num_prenotazioni = new QSpinBox();
        num_prenotazioni->setRange(0,999);
        num_prenotazioni->setValue(tipoAlbAff->getPrenotazioniMeseCorrente());
        QLabel* fondo = new QLabel("Nome fondo:");
        nome_fondo = new QLineEdit();
        nome_fondo->setText(QString::fromStdString(tipoAlbAff->getNomeFondo()));
        QLabel* investimento = new QLabel("Investimento[€]:");
        num_investimento = new QDoubleSpinBox();
        num_investimento->setRange(0.00,999999.99);
        num_investimento->setValue(tipoAlbAff->getInvestimento());
        num_investimento->setSingleStep(0.50);
        QLabel* possesso = new QLabel("Possesso[%]:");
        num_possesso = new QDoubleSpinBox();
        num_possesso->setRange(0.0,100.0);
        num_possesso->setValue(tipoAlbAff->getPercentualePossesso()*100);
        num_possesso->setSingleStep(0.1);
        QLabel* spese = new QLabel("Spese mensili[€]:");
        num_spese = new QDoubleSpinBox();
        num_spese->setRange(0.00,999999.99);
        num_spese->setValue(tipoAlbAff->getSpeseMensili());
        QLabel* agenzia = new QLabel("Agenzia di gestione:");
        nome_agenzia = new QLineEdit();
        nome_agenzia->setText(QString::fromStdString(tipoAlbAff->getAgenziaDiGestione()));
        QLabel* incasso = new QLabel("Incasso mese[€]:");
        num_incasso = new QDoubleSpinBox();
        num_incasso->setRange(0.00,999999.99);
        num_incasso->setValue(tipoAlbAff->getIncassoMeseCorrente());
        num_incasso->setSingleStep(0.50);

        frameLay->addWidget(stanze,0,0,1,1);
        frameLay->addWidget(num_stanze,0,1,1,4);
        frameLay->addWidget(stelle,0,5,1,1);
        frameLay->addWidget(num_stelle,0,6,1,4);
        frameLay->addWidget(colazione,1,0,1,1);
        frameLay->addWidget(haveColazione,1,1,1,4);
        frameLay->addWidget(quota,1,5,1,1);
        frameLay->addWidget(num_quota,1,6,1,4);
        frameLay->addWidget(prenotazioni,2,0,1,1);
        frameLay->addWidget(num_prenotazioni,2,1,1,4);
        frameLay->addWidget(fondo,2,5,1,1);
        frameLay->addWidget(nome_fondo,2,6,1,4);
        frameLay->addWidget(investimento,3,0,1,1);
        frameLay->addWidget(num_investimento,3,1,1,4);
        frameLay->addWidget(possesso,3,5,1,1);
        frameLay->addWidget(num_possesso,3,6,1,4);
        frameLay->addWidget(spese,4,0,1,1);
        frameLay->addWidget(num_spese,4,1,1,4);
        frameLay->addWidget(agenzia,4,5,1,1);
        frameLay->addWidget(nome_agenzia,4,6,1,4);
        frameLay->addWidget(incasso,5,0,1,1);
        frameLay->addWidget(num_incasso,5,1,1,4);
        toAdd->setMinimumSize(650,400);


    }else if(Albergo* tipoAlb= dynamic_cast<Albergo*>(impresa)){

        QLabel* stanze = new QLabel("Num. stanze:");
        num_stanze = new QSpinBox();
        num_stanze->setRange(0,999);
        num_stanze->setValue(tipoAlb->getNumeroStanze());
        QLabel* stelle = new QLabel("Num. stelle:");
        num_stelle = new QSpinBox();
        num_stelle->setRange(1,5);
        num_stelle->setValue(tipoAlb->getStelle());
        QLabel* colazione = new QLabel("Colazione:");
        haveColazione = new QComboBox();
        haveColazione->addItem("No");
        haveColazione->addItem("Si");
        haveColazione->setCurrentIndex(tipoAlb->getColazione());
        QLabel* quota = new QLabel("Quota per prenotazione:");
        num_quota = new QSpinBox();
        num_quota->setRange(0,9999);
        num_quota->setValue(tipoAlb->getQuotaPerPrenotazione());
        QLabel* prenotazioni = new QLabel("Prenotazioni mese corrente");
        num_prenotazioni = new QSpinBox();
        num_prenotazioni->setRange(0,999);
        num_prenotazioni->setValue(tipoAlb->getPrenotazioniMeseCorrente());


        frameLay->addWidget(stanze,0,0,1,1);
        frameLay->addWidget(num_stanze,0,1,1,4);
        frameLay->addWidget(stelle,0,5,1,1);
        frameLay->addWidget(num_stelle,0,6,1,4);
        frameLay->addWidget(colazione,1,0,1,1);
        frameLay->addWidget(haveColazione,1,1,1,4);
        frameLay->addWidget(quota,1,5,1,1);
        frameLay->addWidget(num_quota,1,6,1,4);
        frameLay->addWidget(prenotazioni,2,0,1,1);
        frameLay->addWidget(num_prenotazioni,2,1,1,4);
        toAdd->setMinimumSize(550,200);

    }else if(Appartamento* tipoApp= dynamic_cast<Appartamento*>(impresa)){

        QLabel* metri = new QLabel("mq.:");
        num_metri = new QDoubleSpinBox();
        num_metri->setRange(00.00,999.99);
        num_metri->setValue(tipoApp->getMetriQuadrati());
        num_metri->setSingleStep(0.25);
        QLabel* persone = new QLabel("Numero max persone:");
        num_persone =  new QSpinBox();
        num_persone->setRange(1,10);
        num_persone->setValue(tipoApp->getNumeroMassimoPersone());
        QLabel* valutazione = new QLabel("Valutazione:");
        num_valutazione = new QSpinBox();
        num_valutazione->setRange(1,10);
        num_valutazione->setValue(tipoApp->getValutazione());
        QLabel* tassazione = new QLabel("Tassazione[%]:");
        num_tassazione = new QDoubleSpinBox();
        num_tassazione->setRange(0.0,100.0);
        num_tassazione->setValue(tipoApp->getPercentualeTassazione());
        num_tassazione->setSingleStep(0.1);
        QLabel* costoPrenotazioni = new QLabel("Costo prenotazioni[€]:");
        num_costo = new QDoubleSpinBox();
        num_costo->setRange(0.0,9999.99);
        num_costo->setValue(tipoApp->getCostoPrenotazioniMeseCorrente());


        frameLay->addWidget(metri,0,0,1,1);
        frameLay->addWidget(num_metri,0,1,1,4);
        frameLay->addWidget(persone,0,5,1,1);
        frameLay->addWidget(num_persone,0,6,1,4);
        frameLay->addWidget(valutazione,1,0,1,1);
        frameLay->addWidget(num_valutazione,1,1,1,4);
        frameLay->addWidget(costoPrenotazioni,1,5,1,1);
        frameLay->addWidget(num_costo,1,6,1,4);
        frameLay->addWidget(tassazione,2,0,1,1);
        frameLay->addWidget(num_tassazione,2,1,1,4);
        toAdd->setMinimumSize(550,200);

    }else if( Campeggio* tipoCam= dynamic_cast<Campeggio*>(impresa)){
        QLabel* bagno = new QLabel("Bagno:");
        haveBagno = new QComboBox();
        haveBagno->addItem("No");
        haveBagno->addItem("Si");
        haveBagno->setCurrentIndex(tipoCam->getBagnoPersonale());
        QLabel* tenda = new QLabel("Tenda fornita:");
        haveTenda = new QComboBox();
        haveTenda->addItem("No");
        haveTenda->addItem("Si");
        haveTenda->setCurrentIndex(tipoCam->getTendaFornita());
        QLabel* parcheggio = new QLabel("Costo Parcheggio[€]:");
        costo_parcheggio = new QDoubleSpinBox();
        costo_parcheggio->setRange(0.00,999.99);
        costo_parcheggio->setValue(tipoCam->getCostoParcheggio());
        costo_parcheggio->setSingleStep(0.50);
        QLabel* quotaPersona = new QLabel("Quota Per Persona[€]:");
        costo_persona = new QDoubleSpinBox();
        costo_persona->setRange(0.00,999.99);
        costo_persona->setValue(tipoCam->getQuotaPerPersona());
        costo_persona->setSingleStep(0.50);
        QLabel* ospitanti = new QLabel("Numero ospitanti mese:");
        num_ospitanti = new QSpinBox();
        num_ospitanti->setRange(0,999);
        num_ospitanti->setValue(tipoCam->getNumeroOspitatiMeseCorrente());


        frameLay->addWidget(bagno,0,0,1,1);
        frameLay->addWidget(haveBagno,0,1,1,4);
        frameLay->addWidget(tenda,0,5,1,1);
        frameLay->addWidget(haveTenda,0,6,1,4);
        frameLay->addWidget(parcheggio,1,0,1,1);
        frameLay->addWidget(costo_parcheggio,1,1,1,4);
        frameLay->addWidget(quotaPersona,1,5,1,1);
        frameLay->addWidget(costo_persona,1,6,1,4);
        frameLay->addWidget(ospitanti,2,0,1,1);
        frameLay->addWidget(num_ospitanti,2,1,1,4);
        toAdd->setMinimumSize(550,200);
    }



    frame->setLayout(frameLay);
    toAdd->addItem(frame,"Informazioni specifiche:");
    modifica->setEnabled(true);
    glayout->addWidget(toAdd,2,0,1,10);

}



void ModificaWindow::mod(){

    if(EditNome->text()==""||EditTitolare->text()==""||EditIndirizzo->text()==""){       //controllo che nome, titolare ed indrizzo non siano vuoti. nel caso emetto un error
        QMessageBox* msg = new QMessageBox();
        msg->setIcon(QMessageBox::Critical);
        msg->setWindowTitle("Attenzione");
        msg->setText("Nome, Titolare e Indirizzo non possono essere vuoti");
        msg->exec();
    }
    else{
        impresa->setNome(EditNome->text().toStdString());
        impresa->setCitta(EditCitta->text().toStdString());
        impresa->setTitolare(EditTitolare->text().toStdString());
        impresa->setIndirizzo(EditIndirizzo->text().toStdString());
        impresa->setNazione(EditNazione->text().toStdString());

        if(AlbergoAffiliato* tipoAlbAff= dynamic_cast<AlbergoAffiliato*>(impresa)){

            tipoAlbAff->setNumeroStanze(num_stanze->value());
            tipoAlbAff->setStelle(num_stelle->value());                                                      
            tipoAlbAff->setColazione(haveColazione->currentIndex());            //haveColazione ha index {0,1} => 0(No)=>false, 1(Si)=>true
            tipoAlbAff->setQuotaPerPrenotazione(num_quota->value());
            tipoAlbAff->setPrenotazioniMeseCorrente(num_prenotazioni->value());
            tipoAlbAff->setNomeFondo(nome_fondo->text().toStdString());
            tipoAlbAff->setInvestimento(num_investimento->value());
            tipoAlbAff->setPercentualePossesso(num_possesso->value());
            tipoAlbAff->setSpeseMensili(num_spese->value());
            tipoAlbAff->setAgenziaDiGestione(nome_agenzia->text().toStdString());
            tipoAlbAff->setIncassoMeseCorrente(num_incasso->value());

        }else if(Albergo* tipoAlb= dynamic_cast<Albergo*>(impresa)){

            tipoAlb->setNumeroStanze(num_stanze->value());
            tipoAlb->setStelle(num_stelle->value());
            tipoAlb->setColazione(haveColazione->currentIndex());
            tipoAlb->setQuotaPerPrenotazione(num_quota->value());
            tipoAlb->setPrenotazioniMeseCorrente(num_prenotazioni->value());

        }else if(Appartamento* tipoApp= dynamic_cast<Appartamento*>(impresa)){

            tipoApp->setMetriQuadrati(num_metri->value());
            tipoApp->setNumeroMassimoPersone(num_persone->value());
            tipoApp->setValutazione(num_valutazione->value());
            tipoApp->setPercentualeTassazione(num_tassazione->value());
            tipoApp->setCostoPrenotazioniMeseCorrente(num_costo->value());


        }else if( Campeggio* tipoCam= dynamic_cast<Campeggio*>(impresa)){

            tipoCam->setBagnoPersonale(haveBagno->currentIndex());
            tipoCam->setTendaFornita(haveTenda->currentIndex());
            tipoCam->setCostoParcheggio(costo_parcheggio->value());
            tipoCam->setQuotaPerPersona(costo_persona->value());
            tipoCam->setNumeroOspitatiMeseCorrente(num_ospitanti->value());

        }


        QMessageBox* msg = new QMessageBox();
        msg->setIcon(QMessageBox::Information);
        msg->setWindowTitle("Operazione riuscita");
        msg->setText("Impresa modificata correttamente");
        msg->exec();

        emit listaModificata();
        chiudiFinestra();
    }
}

void ModificaWindow::chiudiFinestra(){
        delete this;
    }


ModificaWindow::~ModificaWindow(){

}

void ModificaWindow::eliminaImpresa(){

    backup->removeItem(impresa);
    QMessageBox* msg = new QMessageBox();
    msg->setIcon(QMessageBox::Information);
    msg->setWindowTitle("Operazione riuscita");
    msg->setText("Impresa rimossa correttamente");
    msg->exec();

    emit listaModificata();
    chiudiFinestra();

}
