#include "contabilitawindow.h"
#include<QString>

ContabilitaWindow::ContabilitaWindow(container* c, QWidget *parent):QWidget(parent), conte(c){

    setWindowTitle("Contabilita'");
    setMinimumSize(QSize(800,500));
    layout = new QGridLayout();

    QLabel* descrizione= new QLabel("CONTABILITA' DELLE IMPRESE SELEZIONATE:");
    QLabel* fatturatoTotale= new QLabel("In totale le imprese selezionate hanno fruttato a Prenotando un totale di: ");
    TotFatturato = new QLineEdit();
    TotFatturato->setReadOnly(true);
    TotFatturato->setText(QString::fromStdString(std::to_string(getFatturatoTotale()) + "€"));
    QLabel* fatturatoMensileTotale= new QLabel("In questo mese è stato generato un profitto di: ");
    TotMensile = new QLineEdit();
    TotMensile->setReadOnly(true);
    TotMensile->setText(QString::fromStdString(std::to_string(getFatturatoMensile()) + "€"));
    QLabel* fatturatoAnnuoDescrizione= new QLabel("Fatturato degli ultimi 12 mesi in ordine cronologico:");
    chiudiMese= new QPushButton(tr("Concludi il mese corrente"));
    tabella = new QTableWidget(12,1);
    riempiTabella();
    tabella->setEditTriggers(QAbstractItemView::NoEditTriggers);

    layout->addWidget(descrizione,0,0,1,1);
    layout->addWidget(chiudiMese,0,1,1,1);
    layout->addWidget(fatturatoTotale,1,0,1,1);
    layout->addWidget(TotFatturato,2,0,1,1);
    layout->addWidget(fatturatoMensileTotale,3,0,1,1);
    layout->addWidget(TotMensile,4,0,1,1);
    layout->addWidget(fatturatoAnnuoDescrizione,5,0,1,1);
    layout->addWidget(tabella,6,0,1,1);
    setLayout(layout);



    //SLOTS

    connect(chiudiMese, SIGNAL(clicked()), this, SLOT(chiudiTuttiMesi()));    //quando viene premuto il pulsante chiudeMese si chiudono i mesi di tutte le imprese nel container

}


ContabilitaWindow::~ContabilitaWindow()
{
}



double ContabilitaWindow::getFatturatoTotale() const{
    double fatturatoTot=100;
    for(container::const_iterator cit = conte->cbegin(); cit!= conte->cend(); cit++){
               fatturatoTot += (*cit)->fatturatoTotale();
    }
    return fatturatoTot;
}




double ContabilitaWindow::getFatturatoMensile() const{
    double fatturatoMen=0;
    for(container::const_iterator cit = conte->cbegin(); cit!= conte->cend(); cit++){
               fatturatoMen += (*cit)->fatturatoMeseCorrente();
    }
    return fatturatoMen;
}


QString ContabilitaWindow::getFatturatoPerMese(int mese) const{
    double incasso=0;
    for(container::const_iterator cit = conte->cbegin(); cit!= conte->cend(); cit++){
        incasso+=(*cit)->getFatturatoPerMese(mese);             //getFatturatoPerMese() è un metodo virtuale puro di ImpresaRicettiva
    }
    QString fatturatoPerM = QString::fromStdString(std::to_string(incasso)+"€");
    return fatturatoPerM;
}


void ContabilitaWindow::chiudiTuttiMesi(){
    for(container::const_iterator cit = conte->cbegin(); cit!= conte->cend(); cit++){
               (*cit)->chiudiMese();
    }
    impostaDati();

    QMessageBox* msg = new QMessageBox();
    msg->setIcon(QMessageBox::Information);
    msg->setWindowTitle("Operazione riuscita");
    msg->setText("Mese concluso correttamente");
    msg->exec();
}

void ContabilitaWindow::riempiTabella(){
    tabella->setHorizontalHeaderItem(0, new QTableWidgetItem("Fatturato"));
    for(int mese=0; mese<12; ++mese){
        tabella->setItem(mese,0,new QTableWidgetItem(getFatturatoPerMese(mese)));
    }
}

void ContabilitaWindow::impostaDati(){
    TotFatturato->setText(QString::fromStdString(std::to_string(getFatturatoTotale())+"€"));
    TotMensile->setText(QString::fromStdString(std::to_string(getFatturatoMensile())+"€"));
    for(int mese=0; mese<12; ++mese){
        tabella->setItem(mese,0,new QTableWidgetItem(getFatturatoPerMese(mese)));
    }
}
